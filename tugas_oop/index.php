<?php
require_once("Animal.php");
require_once("Ape.php");
require_once("Frog.php");
$sheep = new Animal("shaun");

echo "Nama Hewan : $sheep->name<br>"; // "shaun"
echo "Jumlah Kaki :$sheep->legs<br>"; // 2
echo "Cold Blood : $sheep->cold_blooded<br>"; // false

$sungokong = new Ape("kera sakti");
echo "Nama : $sungokong->name<br>";
$sungokong->yell(); // "Auooo"

$kodok = new Frog("buduk");
echo "Nama : $kodok->name<br>";
$kodok->jump(); // "hop hop"