<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', [HomeController::class, 'index']);
Route::get('register', [AuthController::class, 'register']);
Route::post('/welcome', [HomeController::class, 'welcome']);
Route::resource('pertanyaan', 'PertanyaanController');
// Route::get('data-tables', function () {
//     return view('pages.data-table');
// });
// Route::get('pertanyaan', 'PertanyaanController@index');
// Route::post('pertanyaan', 'PertanyaanController@store');
// Route::get('pertanyaan/create', 'PertanyaanController@create');
// Route::get('pertanyaan/{pertanyaan_id}', 'PertanyaanController@show');
// Route::get('/pertanyaan/{pertanyaan_id}/edit','PertanyaanController@edit');
// Route::put('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@update');
// Route::delete('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@destroy');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
