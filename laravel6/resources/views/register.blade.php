<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Page</title>
</head>
<body>
    <h1>Form Pendaftaran</h1>
    <form action="welcome" method="post">
        @csrf
        <label>First Name</label><br><br>
        <input type="text" name="first_name"><br><br>
        <label>Last Name</label><br><br>
        <input type="text" name="last_name"><br><br>
        <label>Gender</label><br><br>
        <input type="radio" name="gender" value="Male">Male
        <input type="radio" name="gender"value="Female">Female<br><br>
        <label>Tanggal Lahir</label><br><br>
        <input type="date" name="ttl"> 
        <label>Warga Negara</label><br><br>
        <input type="radio" name="WN">WNA
        <input type="radio" name="WN">WNI
        <br>  <br>
        <label>Skill</label>
        <input type="checkbox">HTML
        <input type="checkbox">CSS
        <input type="checkbox">JAVASCRIPT
        <input type="checkbox">LARAVEL <br> <br>
        <label>Jenis Kelamin</label> <br>
        <br>
        <select name="JK">
            <option value="laki-laki">Laki-Laki</option>
            <option value="perempuan">Perempuan</option>
        </select>
        <label>Alamat</label><br><br>
        <textarea name="alamat" cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="Daftar">
    </form>
</body>
</html>