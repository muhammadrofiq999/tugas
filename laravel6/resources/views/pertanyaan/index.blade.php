@extends('pertanyaan.master')
@section('title', 'Index')
@section('content')
    <a href="/pertanyaan/create" class="btn btn-primary d-block mb-3">Tambah</a>
    <div class="row">
        @forelse ($pertanyaan as $key=>$value)
            <div class="card m-3" style="width: 18rem;">
                <div class="card-header">
                    <h5 class="card-title">{{ $value->judul }}</h5>
                </div>
                <div class="card-body">
                    <p class="card-text">{{ $value->isi }}</p>
                </div>
                <ul class="list-group list-group-flush">
                    <h5 class="pl-3">Jawaban : </h5>
                    <li class="list-group-item">Cras justo odio</li>
                    <li class="list-group-item">Dapibus ac facilisis in</li>
                    <li class="list-group-item">Vestibulum at eros</li>
                </ul>
                <div class="card-body">
                    <div class="d-flex justify-content-around">
                        <a href="/pertanyaan/{{ $value->id }}" class="card-link">Show</a>
                        <a href="/pertanyaan/{{ $value->id }}/edit" class="card-link">Edit</a>
                    </div>
                    <form action="/pertanyaan/{{ $value->id }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger form-control" value="Delete">
                    </form>
                </div>
            </div>
        @empty
            <h3>no data</h3>
        @endforelse
    </div>
@endsection
