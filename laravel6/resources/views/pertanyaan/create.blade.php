@extends('pertanyaan.master')
@section('title', 'Create')
@section('content')
    <h2>Tambah Data</h2>
    <form action="/pertanyaan" method="post">
        @csrf
        <div class="form-group">
            <label for="title">Penanya</label>
            <select class="form-control" name="penanya" id="penanya">
                @forelse ($profil as $profils)
                    <option value="{{ $profils->id }}">{{ $profils->nama_lengkap }}</option>
                @empty
                    <option value="">No Data</option>
                @endforelse
            </select>
            @error('title')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="title">Judul</label>
            <input type="text" class="form-control" name="judul" id="Judul" placeholder="Masukkan Judul">
            @error('judul')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">Isi</label>
            <input type="text" class="form-control" name="isi" id="isi" placeholder="Masukkan isi">
            @error('isi')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
    </div>
@endsection
