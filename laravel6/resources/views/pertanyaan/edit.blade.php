@extends('pertanyaan.master')
@section('title', 'Create')
@section('content')
    <h2>Edit Data</h2>
    <form action="/pertanyaan/{{ $pertanyaan->id }}" method="post">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="title">Penanya</label>
            <select class="form-control" name="penanya" id="penanya">
                <option value="{{ $pertanyaan->profil_id }}">{{ $profil->nama_lengkap }}</option>
            </select>
            @error('title')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="title">Judul</label>
            <input type="text" class="form-control" name="judul" id="Judul" value="{{ $pertanyaan->judul }}"
                placeholder="Masukkan Judul">
            @error('judul')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">Isi</label>
            <input type="text" class="form-control" name="isi" id="isi" value=" {{ $pertanyaan->isi }}"
                placeholder="Masukkan isi">
            @error('isi')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
    </div>
@endsection
