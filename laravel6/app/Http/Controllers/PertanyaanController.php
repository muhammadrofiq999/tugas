<?php

namespace App\Http\Controllers;

use App\Pertanyaan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PertanyaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $pertanyaan = Pertanyaan::get();
        return view('pertanyaan.index', ['pertanyaan' => $pertanyaan]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $profil = DB::table('profil')->get();
        return view('pertanyaan.create', ['profil' => $profil]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'penanya' => 'required',
            'judul' => 'required',
            'isi' => 'required',
        ]);
        $query = Pertanyaan::insert([
            "profil_id" => $request->penanya,
            "judul" => $request->judul,
            "isi" => $request->isi
        ]);
        return redirect('/pertanyaan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pertanyaan = Pertanyaan::where(['id' => $id])->get();
        return view('pertanyaan.index', ['pertanyaan' => $pertanyaan]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pertanyaan = Pertanyaan::where('id', $id)->first();
        $profil = DB::table('profil')->where('id', $pertanyaan->profil_id)->first();
        return view('pertanyaan.edit', compact('pertanyaan', 'profil'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'penanya' => 'required',
            'judul' => 'required',
            'isi' => 'required',
        ]);

        $query = Pertanyaan::where('id', $id)
            ->update([
                "judul" => $request->judul,
                "isi" => $request->isi
            ]);
        return redirect('/pertanyaan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $query = Pertanyaan::where('id', $id)->delete();
        return redirect('/pertanyaan');
    }
}
