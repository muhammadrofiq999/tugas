<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    protected $table="pertanyaan";
    protected $fillable=['profil_id','jawaban_tepat_id','judul','isi'];
}
